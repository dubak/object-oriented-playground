<?php

class PrvaTrieda {
	
	/** @var integer  */
	private $hodnotaDana = 10;

	/** @var int */
	public $hodnotaZiskana = '';
	
	/** 
	 * @desc konstruktor objektu
	 * @param void 
	 * @return void
	 */
	function __construct(){
		// nastavim hodnotu premennej
		$this ->setPremennu();
	}
	
	/** 
	 * @desc setter hodnoty premennej
	 * @param void 
	 * @return void
	 */
	protected function setPremennu() {
		$this->hodnotaZiskana = $this->hodnotaDana;
	}
	
	/** 
	 * @desc getter hodnoty premennej 
	 * @param void 
	 * @return integer 
	 */
	public function getPremennu() {
		return $this->hodnotaZiskana;
	}
	
}


$inst = new PrvaTrieda();

print_r($inst);

echo $inst -> getPremennu();



