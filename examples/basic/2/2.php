<?php

class PrvaTrieda {
	
	/** @var integer  */
	protected $hodnotaDana = 10;
				
}

class DruhaTrieda extends PrvaTrieda  {


	/** @var int */
	public $hodnotaZiskana = '';

	/**
	 * @desc konstruktor objektu
	 * @param void
	 * @return void
	 */
	function __construct(){
		// nastavim hodnotu premennej
		$this ->setPremennu();
	}

	/**
	 * @desc setter hodnoty premennej
	 * @param void
	 * @return void
	 */
	protected function setPremennu() {
		$this->hodnotaZiskana = parent::$this->hodnotaDana;
	}

	/**
	 * @desc getter hodnoty premennej
	 * @param void
	 * @return integer
	 */
	public function getPremennu() {
		return $this->hodnotaZiskana;
	}

}

print_r(new PrvaTrieda());

$inst = new DruhaTrieda();

print_r($inst);

echo $inst -> getPremennu();



