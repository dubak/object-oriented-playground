<?php


interface IVersion 
{	
	public function beforeLoad ( );
	
	public function afterLoad ( );
				
}


class version1 implements IVersion
{
	protected $response = [];

	public function getResponse ( )
	{
		try {
		
			if (!$this instanceof IVersion) {
				throw new Exception('Not instance!');
			}

			$result = '';

			foreach ($this->response as $key => $value) {
				$result .= '<pre>'.$key.' => '.$value.'</pre>';
			}		
			//$result = '<pre>'.print_r($this->response).'</pre>';
		
			return $result;
		}
		catch ( Exception $e ) {
			echo $e->getMessage();
		}
	}	
	
	public function beforeLoad ( ) 
	{
		
	}
	
	public function generateResponse ( $id )
	{
		$this->response[$id] = $id;
		$this->response['title'] = __CLASS__ . ' - ' . __FUNCTION__;
	}
	
	public function afterLoad ( ) 
	{
		
	}
}


class version2 extends version1 
{
	public function generateResponse ( $id )
	{
		$this->response[$id] = $id;
		$this->response['title'] = __CLASS__ . ' - ' . __FUNCTION__;
	}
	
}


$version = new version1();
$version->generateResponse(1);
echo $version->getResponse();


$version = new version2();
$version->generateResponse(2);
echo $version->getResponse();
