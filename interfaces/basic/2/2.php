<?php

interface UserType {	
	public abstract function verify ( $data );			
}

class Gmail implements UserType 
{
	private $profile = [];
	
	private $logged = false;
	
	public function __construct ( ) 
	{
	}
	
	public function verify ( array $data)
	{
		// do verification		
		$this->logged = true;
		$this->profile = ['gmail' => ['nick' => 'John', 'age' => 31] ];		
	}
	
	public function getProfile ( )
	{
		return $this->profile;
	}	
	
	public function isLogged ()
	{
		return $this->logged;
	}		
}

class Facebook implements UserType 
{
	private $profile = [];
	
	private $logged = false;
	
	public function __construct ( ) 
	{
	}
	
	public function verify ( array $data)
	{
		// do verification		
		$this->logged = true;
		$this->profile = ['facebook' => ['nick' => 'Jane', 'age' => 26] ];		
	}	
	
	public function getProfile ( )
	{
		return $this->profile;
	}	
	
	public function isLogged ()
	{
		return $this->logged;
	}	
}

class UserVerification {
	
	private static $profile = [];
	private static $logged = false;
	
	private function __construct ( ) { }
	
	public static function verify ( $type, $data )
	{
		switch ($type) {
			case 'Gmail':
				$gmail = new Gmail();
				$gmail->verify($type, $data);
				self::$profile = $gmail->getProfile();
				self::$logged = $gmail->isLogged();
			break;

			case 'Facebook':
				$facebook = new Facebook();
				$facebook->verify($type, $data);
				self::$profile = $facebook->getProfile();				
				self::$logged = $facebook->isLogged();				
			break;
		}
	}
}

class Discussion {
	
	/**
	 * @var boolean 
	 * @access private
	 */
	private $verificationState = false;
	
	private $userProfile = [];
		
	private function verifyUser ( $type, $data )
	{
		UserVerification::verify($type, $data);
		$this->verificationState = UserVerification::getState();
		$this->userProfile = UserVerification::getProfile();
	}	
		
	/** 
	 * @param 
	 * @return void
	 */
	function __construct($type, $data)
	{		
		$this->verifyUser($type, $data);		
	}		
}


$inst = new Discussion('facebook', ['hash'=>'132432rg82fggrwg']);

print_r($inst);




