<?php
namespace base;

/**
 * @author David Dubovsky <http://www.dubak.sk>
 */
class Trieda {

	private $class = null;
	
	private $className = null;
	
	private $action = true;
	
	/** 
	 * @access public
	 * @param string $version
	 */
	public function __construct($version, $className)
	{		
		$this->loader($version, $className);
	}
	
	/** 
	 * @access private
	 * @param string $file File name
	 */
	private function loader ($file, $className) 
	{
		$path = $file.'.php';
		if ( file_exists($path)  )
		{	
			require($path);
			
			$explode = explode('.', $file);
			$this->className = $explode[0].'\\'.$className;
			
			$classExists = class_exists($this->className, true);

			if ($classExists) {
				$this->class = $this->className;				  
			}			
		}
		else {
			$this->action = false;
		}
	}
			
	/** 
	 */
	public function says ( ) 
	{
		$class = $this->class;
		return $class::says();
	}
	
}

$version = 'v1.0';
$inst = new Trieda($version, 'Animal');
echo $inst->says();
echo '<br />';
$inst = new Trieda('v2.0', 'Animal');
echo $inst->says();
